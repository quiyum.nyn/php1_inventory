<?php
session_start();
include_once("templateLayout/templateInfo.php");
include_once "../src/Product.php";
$productObj=new Product();
$productData=$productObj-> showProduct();
include_once "../src/Temp.php";
$tempObj=new Temp();
$is_exist=$tempObj->if_exist();
$vendor=$tempObj->showVendor();
$allTemp=$tempObj->showTemp();
$total_price=$tempObj->totalPrice();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title;?></title>
    <?php include_once("templateLayout/css.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <?php include_once("templateLayout/header.php") ?>
        <!-- script-for sticky-nav -->
        <?php include_once("templateLayout/script.php") ?>
        <!-- /script-for sticky-nav -->
        <!--inner block start here-->
        <div class="inner-block" style="min-height: 700px">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                    <p class="text-center"><?php if(isset($_SESSION['message'])){echo $_SESSION['message']; $_SESSION['message']="";} ?></p>
                    <div class="login-block">
                        <form action="../controller/addProduct.php" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="vendor_name" placeholder="Vendor Name" <?php if($is_exist){echo "value='$vendor->vendor_name'"; echo "readonly";} ?> required="">
                                </div>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="vendor_contact" placeholder="Vendor Contact"  <?php if($is_exist){echo "value='$vendor->vendor_contact'"; echo "readonly";} ?> required="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <select class="form-control" name="product_id">
                                        <?php
                                        foreach($productData as $oneData){
                                            echo "<option value='$oneData->id' >$oneData->product_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input type="number" class="form-control" name="quantity" placeholder="Quantity" required="">
                                </div>
                                <div class="col-md-3">
                                    <input type="number" class="form-control" name="price" placeholder="Price" required="">
                                </div>
                            </div>
                            <br>
                            <input type="submit" value="Add Another Product">
                        </form>
                        <br><br> <br><br>
                        <form action="../controller/purchase.php" method="post">
                            <table id="example" class="table table-bordered" >
                                <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                    <th>Total Price</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $serial=1;
                                foreach($allTemp as $oneData){
                                    ?>

                                    <tr>
                                        <td><?php echo $serial;?></td>
                                        <td><?php echo $oneData->product_name;?></td>
                                        <td><?php echo $oneData->quantity;?></td>
                                        <td><?php echo $oneData->unit_name;?></td>
                                        <td><?php echo $oneData->price;?></td>
                                        <td><?php echo $oneData->total_price;?></td>
                                        <td><a href='editProduct.php?id=$oneData->id' class='btn btn-primary' style='width:50%; float:left'>Edit</a><a href='deleteProduct.php?id=$oneData->id' class='btn btn-warning' style='width:50%; float:right'>Delete</a></td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }
                                ?>

                                </tbody>
                            </table> <br>
                         
                            <div class="row">
                                <div class="col-md-5">
                                    <label>Total Price</label>
                                    <input type="number" class="form-control" name="total_price" value="<?php echo $total_price->total_price?>" readonly>
                                </div>
                                <div class="col-md-5">
                                    <label>Payment</label>
                                    <input type="number" class="form-control" name="payment" max="<?php echo $total_price->total_price?>" placeholder="Payment">
                                </div>
                                <?php
                                foreach($allTemp as $oneData) {
                                    ?>
                                    <input type="hidden" name="product_id[]" value="<?php echo $oneData->product_id; ?>">
                                    <input type="hidden" name="quantity[]" value="<?php echo $oneData->quantity; ?>">
                                    <input type="hidden" name="price[]" value="<?php echo $oneData->price; ?>">
                                    <input type="hidden" name="total[]" value="<?php echo $oneData->total_price; ?>">
                                    <?php
                                }
                                ?>
                                <div class="col-md-2">
                                    <label></label>
                                    <input type="hidden" name="vendor_name" value="<?php echo $vendor->vendor_name;?>">
                                    <input type="hidden" name="vendor_contact" value="<?php echo $vendor->vendor_contact;?>">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--inner block end here-->
        <?php include_once("templateLayout/footer.php");?>
    </div>

    <!--slider menu-->
    <?php include_once("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php include_once("templateLayout/script.php");?>
</body>
</html>




