<?php
include_once("templateLayout/templateInfo.php");
include_once "../src/Category.php";
$object=new Category();
$allData=$object-> showCategory();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title;?></title>
    <?php include_once("templateLayout/css.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <?php include_once("templateLayout/header.php") ?>
        <!-- script-for sticky-nav -->
        <?php include_once("templateLayout/script.php") ?>
        <!-- /script-for sticky-nav -->
        <!--inner block start here-->
        <div class="inner-block" style="min-height: 700px">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="login-block">

                        <form action="../controller/createCategory.php" method="post">
                            <input type="text" name="category_name" placeholder="Category Name" required="">
                            <br>
                            <input type="text" name="category_desc" placeholder="Category Description" required="">
                            <br>
                            <input type="submit" value="Add Category">
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 23px">
                    <h4 class="text-center">Category Lookup Table</h4><br>
                    <table class="table table-bordered table-responsive table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Category Name</th>
                            <th>Category Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $serial=1;
                        foreach ($allData as $oneData){
                            echo "<tr>
                                        <td> $serial</td>
                                        <td> $oneData->category_name</td>
                                        <td> $oneData->category_desc</td>
                                        <td> <a href='editCategory.php?id=$oneData->id' class='btn btn-primary'>Edit</a></td>
                                    </tr>";
                            $serial++;
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--inner block end here-->
        <?php include_once("templateLayout/footer.php");?>
    </div>

    <!--slider menu-->
    <?php include_once("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php include_once("templateLayout/script.php");?>
</body>
</html>




