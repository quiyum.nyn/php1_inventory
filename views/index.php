<?php 
session_start();
include_once("templateLayout/templateInfo.php");
if($_SESSION['status']==1){
    include_once "../src/Login_info.php";
    $authentication=new Login_info();
    $authentication->prepareData($_SESSION);
    $check=$authentication->logged_in();
    if(!$check){
        $_SESSION['message']="Please Login First";
        header("Location: login.php");
    }
}
else{
    $_SESSION['message']="Please Login First";
    header("Location: login.php");
}
?>

<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title; ?></title>
   <?php include_once("templateLayout/css.php"); ?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <!--header start here-->
            <?php include_once("templateLayout/header.php"); ?>
            <div class="inner-block" style="min-height:600px";>
                <p class="text-center"><?php if(isset($_SESSION['message'])){echo $_SESSION['message']; $_SESSION['message']="";} ?></p>
                <div class="market-updates">
                    <div class="col-md-4 market-update-gd">
                        <div class="market-update-block clr-block-1">
                            <div class="col-md-8 market-update-left">
                                <h3>83</h3>
                                <h4>Registered User</h4>
                                <p>Other hand, we denounce</p>
                            </div>
                            <div class="col-md-4 market-update-right">
                                <i class="fa fa-file-text-o"> </i>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="col-md-4 market-update-gd">
                        <div class="market-update-block clr-block-2">
                            <div class="col-md-8 market-update-left">
                                <h3>135</h3>
                                <h4>Daily Visitors</h4>
                                <p>Other hand, we denounce</p>
                            </div>
                            <div class="col-md-4 market-update-right">
                                <i class="fa fa-eye"> </i>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="col-md-4 market-update-gd">
                        <div class="market-update-block clr-block-3">
                            <div class="col-md-8 market-update-left">
                                <h3>23</h3>
                                <h4>New Messages</h4>
                                <p>Other hand, we denounce</p>
                            </div>
                            <div class="col-md-4 market-update-right">
                                <i class="fa fa-envelope-o"> </i>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <!--market updates end here-->
                <!--mainpage chit-chating-->


            </div>
            <!--inner block end here-->
            <!--copy rights start here-->
           <?php include_once("templateLayout/footer.php"); ?>
            <!--COPY rights end here-->
        </div>
    </div>
    <!--slider menu-->
    <?php include_once("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php include_once("templateLayout/script.php");?>
</body>
</html>                     