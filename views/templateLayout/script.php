<script src="<?php echo $base_url?>resources/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?php echo $base_url?>resources/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $base_url?>resources/datatables/dataTables.bootstrap.min.js"></script>

<script>
    $(function () {
        $("#example").DataTable();
        $("#paid").DataTable();
        $("#unpaid").DataTable();
        $("#example2").DataTable();
        $('#example3').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>


<script>
    $(document).ready(function() {
        var navoffeset=$(".header-main").offset().top;
        $(window).scroll(function(){
            var scrollpos=$(window).scrollTop();
            if(scrollpos >=navoffeset){
                $(".header-main").addClass("fixed");
            }else{
                $(".header-main").removeClass("fixed");
            }
        });

    });
</script>
<script>
    var toggle = true;

    $(".sidebar-icon").click(function() {
        if (toggle)
        {
            $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
            $("#menu span").css({"position":"absolute"});
        }
        else
        {
            $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
            setTimeout(function() {
                $("#menu span").css({"position":"relative"});
            }, 400);
        }
        toggle = !toggle;
    });
</script>
<!--scrolling js-->
<script src="<?php echo $base_url?>/resource/js/jquery.nicescroll.js"></script>
<script src="<?php echo $base_url?>/resource/js/scripts.js"></script>
<!--//scrolling js-->
<script src="<?php echo $base_url?>/resource/js/bootstrap.js"> </script>
<!-- mother grid end here-->

<link rel="stylesheet" type="text/css" href="<?php echo $base_url?>/resource/css/magnific-popup.css">
<script type="text/javascript" src="<?php echo $base_url?>/resource/js/nivo-lightbox.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#nivo-lightbox-demo a').nivoLightbox({ effect: 'fade' });
    });
</script>