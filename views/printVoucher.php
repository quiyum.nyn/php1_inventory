<?php
session_start();
include_once("templateLayout/templateInfo.php");
include_once "../src/Purchase_master.php";
include_once "../src/Purchase_details.php";
include_once "../src/Purchase_bill.php";
$purBillObj=new Purchase_bill();
$masterObj=new Purchase_master();
$dObject=new Purchase_details();
$masterObj->prepareData($_GET);
$dObject->prepareData($_GET);
$purBillObj->prepareData($_GET);
$masterData=$masterObj->showOneDetails();
$details=$dObject->showDetails();
$purBillDetails=$purBillObj->showBill();
$date=date("d-M-Y",strtotime($masterData->date));
$time=date("h:i a",strtotime($masterData->date));

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title;?></title>
    <?php include_once("templateLayout/css.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <?php include_once("templateLayout/header.php") ?>
        <!-- script-for sticky-nav -->
        <?php include_once("templateLayout/script.php") ?>
        <!-- /script-for sticky-nav -->
        <!--inner block start here-->
        <div class="inner-block" style="min-height: 700px">
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12">
                    <p class="text-center"><?php if(isset($_SESSION['message'])){echo $_SESSION['message']; $_SESSION['message']="";} ?></p>
                    <div class="login-block">
                        <div class="row">
                           <h4 class="text-center">Voucher</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label>MRR No</label>
                                <input type="text" class="form-control" value="<?php echo $masterData->id; ?>  ">
                            </div>
                            <div class="col-md-2">
                                <label>Date</label>
                                <input type="text" class="form-control" value="<?php echo $date;?> ">
                            </div>
                            <div class="col-md-2">
                                <label>Time</label>
                                <input type="text" class="form-control" value="<?php echo $time; ?> ">
                            </div>

                            <div class="col-md-3">
                                <label>Vendor Name</label>
                                <input type="text" class="form-control" value=" <?php echo $masterData->vendor_name ?>">
                            </div>
                            <div class="col-md-3">
                                <label>Vendor Contact</label>
                                <input type="text" class="form-control" value="<?php echo $masterData->vendor_contact ?>">
                            </div>
                            <div class="col-md-2">
                                <label>Total</label>
                                <input type="text" class="form-control" value="<?php echo $masterData->total; ?> ">
                            </div>

                            <div class="col-md-2">
                                <label>Payment</label>
                                <input type="text" class="form-control" value="<?php echo $masterData->payment ?>">
                            </div>
                            <div class="col-md-2">
                                <label>Due</label>
                                <input type="text" class="form-control" value="<?php echo $masterData->due ?>">
                            </div>
                        </div>
                        <br><hr>
                        <div>
                            <div class="">
                                <h3 class="text-center">Purchase Details</h3>
                                <table id="example" class="table table-bordered" >
                                    <thead>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Unit</th>
                                        <th>Price</th>
                                        <th>Total Price</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $serial=1;
                                    foreach($details as $oneData){
                                        ?>

                                        <tr>
                                            <td><?php echo $serial;?></td>
                                            <td><?php echo $oneData->product_name;?></td>
                                            <td><?php echo $oneData->quantity;?></td>
                                            <td><?php echo $oneData->unit_name;?></td>
                                            <td><?php echo $oneData->price;?></td>
                                            <td><?php echo $oneData->total_price;?></td>
                                        </tr>
                                        <?php
                                        $serial++;
                                    }
                                    ?>

                                    </tbody>
                                </table> <br>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--inner block end here-->
        <?php include_once("templateLayout/footer.php");?>
    </div>

    <!--slider menu-->
    <?php include_once("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php include_once("templateLayout/script.php");?>
</body>
</html>




