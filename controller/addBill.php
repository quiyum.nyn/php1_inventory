<?php
session_start();
include_once "../src/BillTemp.php";
include_once "../src/Stock.php";
$stockObj=new Stock();
$stockObj->prepareData($_POST);
$oneData=$stockObj->showOneStock();
if($oneData->balance_quantity>=$_POST['quantity']){
    $billTempObj=new BillTemp();
    $billTempObj->prepareData($_POST);
    $billTempObj->insertData();
    $_SESSION['message']=" Sale Successfully.";
    header("Location: ../views/bill.php");
}
else{
    $_SESSION['message']="Stock is not available. Total stocked product : ".$oneData->balance_quantity;
    header("Location: ../views/bill.php");
}
