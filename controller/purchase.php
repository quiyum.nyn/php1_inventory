<?php
session_start();
include_once "../src/Purchase_master.php";
include_once "../src/Purchase_details.php";
include_once "../src/Purchase_bill.php";
include_once "../src/Temp.php";
$tempObj=new Temp();
$masterObj=new Purchase_master();
$detailsObj=new Purchase_details();
$billObj=new Purchase_bill();
$_POST['due']=($_POST['total_price'])-($_POST['payment']);
$_POST['new_product_id']=implode(",",$_POST['product_id']);
$_POST['new_quantity']=implode(",",$_POST['quantity']);
$_POST['new_price']=implode(",",$_POST['price']);
$_POST['new_total']=implode(",",$_POST['total']);
$masterObj->prepareData($_POST);
$detailsObj->prepareData($_POST);
$billObj->prepareData($_POST);
$masterObj->insertData();
$billObj->insertData();
$detailsObj->insertData();
$tempObj->delete();
$_SESSION['message']="Purchase has been done Successfully";
header("Location: ../views/purchase_lookup.php");


