<?php
session_start();
include_once "../src/Product.php";
$object=new Product();
$object->prepareData($_POST);
$if_exist=$object->if_exist();
if($if_exist){
    $_SESSION['message']="This Product is already exist";
    header("Location:../views/product.php");
}
else{
    $object->prepareData($_POST);
    $object->insertProduct();
    $_SESSION['message']="This Product is added successfully";
    header("Location:../views/product.php");
}
