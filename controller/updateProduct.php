<?php
session_start();
include_once "../src/Product.php";
$object=new Product();
$object->prepareData($_POST);
$object->updateProduct();
$_SESSION['message']="Your product updated successfully";
header("Location:../views/editProduct.php?id=".$object->id);
