<?php
session_start();
include_once "../src/Purchase_master.php";
include_once "../src/Purchase_bill.php";
$object=new Purchase_master();
$pBillObj=new Purchase_bill();
$_POST['new_total_payment']=$_POST['payment']+$_POST['new_payment'];
$_POST['new_due']=$_POST['total']-$_POST['new_total_payment'];
$object->prepareData($_POST);
$pBillObj->prepareData($_POST);
$object->updatePayment();
$pBillObj->newInsertData();
$_SESSION['message']="Payment successfully";
header("Location:../views/purchase_details.php?id=".$object->id);