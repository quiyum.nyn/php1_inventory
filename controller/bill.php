<?php
session_start();
include_once "../src/Bill_master.php";
include_once "../src/Bill_details.php";
include_once "../src/Pay_bill.php";
include_once "../src/BillTemp.php";
$tempObject=new BillTemp();
$bMasterObj=new Bill_master();
$bDetailsObj=new Bill_details();
$payBillObject=new Pay_bill();
$_POST['due']=($_POST['total_price'])-($_POST['payment']);
$_POST['new_product_id']=implode(",",$_POST['product_id']);
$_POST['new_quantity']=implode(",",$_POST['quantity']);
$_POST['new_price']=implode(",",$_POST['price']);
$_POST['new_total']=implode(",",$_POST['total']);
$bMasterObj->prepareData($_POST);
$bDetailsObj->prepareData($_POST);
$payBillObject->prepareData($_POST);
$bMasterObj->insertData();
$payBillObject->insertBillData();
$bDetailsObj->insertData();
$tempObject->delete();
$_SESSION['message']="Purchase has been done Successfully";
header("Location: ../views/bill_lookup.php");


