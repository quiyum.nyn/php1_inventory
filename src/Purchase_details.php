<?php

include_once "Database.php";
class Purchase_details extends Database
{
    public $id;
    public $purchase_master_id;
    public $quantity;
    public $product_id;
    public $price;
    public $total_price;

    public function __construct(){

        parent:: __construct();
    }
    public function prepareData($data){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists("new_product_id",$data)){
            $this->product_id=$data['new_product_id'];
        }
        if(array_key_exists("new_quantity",$data)){
            $this->quantity=$data['new_quantity'];
        }
        if(array_key_exists("new_price",$data)){
            $this->price=$data['new_price'];
        }
        if(array_key_exists("new_total",$data)){
            $this->total_price=$data['new_total'];
        }
        return $this;
    }
    public function insertData(){
        $select_master_id="SELECT id FROM `purchase_master` ORDER BY id DESC LIMIT 1";
        $STH=$this->DBH->query($select_master_id);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->purchase_master_id=$row['id'];

        $prod_id=explode(",",$this->product_id);
        $quantity=explode(",",$this->quantity);
        $price=explode(",",$this->price);
        $total_price=explode(",",$this->total_price);

        $count=0;
        foreach ($prod_id as $a){
            $query= "INSERT INTO `purchase_details`(`purchase_master_id`, `product_id`, `quantity`, `price`, `total_price`) VALUES (?,?,?,?,?)";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1,$this->purchase_master_id);
            $STH->bindParam(2,$prod_id[$count]);
            $STH->bindParam(3,$quantity[$count]);
            $STH->bindParam(4,$price[$count]);
            $STH->bindParam(5,$total_price[$count]);
            $STH->execute();
            $count++;
        }
    }
    public function showDetails(){
        $sql = "SELECT purchase_details.*,product.product_name,unit_lookup.unit_name FROM `purchase_details`,product,unit_lookup WHERE purchase_details.product_id=product.id AND product.unit_id=unit_lookup.id AND purchase_details.purchase_master_id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

}