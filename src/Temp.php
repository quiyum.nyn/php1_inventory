<?php
include_once 'Database.php';
class Temp extends Database
{
    public $id;
    public $vendor_name;
    public $vendor_contact;
    public $product_id;
    public $quantity;
    public $price;
    public $total;

    public function __construct()
    {
        parent:: __construct();
    }

    public function prepareData($data){
        if(array_key_exists("vendor_name",$data)){
            $this->vendor_name=$data['vendor_name'];
        }
        if(array_key_exists("vendor_contact",$data)){
            $this->vendor_contact=$data['vendor_contact'];
        }
        if(array_key_exists("product_id",$data)){
            $this->product_id=$data['product_id'];
        }
        if(array_key_exists("quantity",$data)){
            $this->quantity=$data['quantity'];
        }
        if(array_key_exists("price",$data)){
            $this->price=$data['price'];
        }
        return $this;
    }
    public function insertData(){
        $this->total=$this->quantity*$this->price;
        $query= "INSERT INTO `temp`(`vendor_name`, `vendor_contact`, `product_id`, `quantity`, `price`, `total_price`) VALUES (?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->vendor_name);
        $STH->bindParam(2,$this->vendor_contact);
        $STH->bindParam(3,$this->product_id);
        $STH->bindParam(4,$this->quantity);
        $STH->bindParam(5,$this->price);
        $STH->bindParam(6,$this->total);
        $STH->execute();
    }
    public function if_exist(){
        $sql = "SELECT * FROM `temp`";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetch();
        $count=$STH->rowCount();

        if($count){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    public function showVendor(){
        $sql = "SELECT vendor_name,vendor_contact FROM `temp` GROUP BY vendor_name AND vendor_contact";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showTemp(){
        $sql = "SELECT temp.*,product.product_name,unit_lookup.unit_name FROM `temp`,product,unit_lookup WHERE product.id=temp.product_id AND unit_lookup.id=product.unit_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function totalPrice(){
        $sql = "SELECT sum(total_price) as total_price FROM temp";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function delete(){
        $query="DElETE FROM temp";
        $this->DBH->exec($query);
    }
    

}
