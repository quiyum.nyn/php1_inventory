<?php
include_once "Database.php";
class Bill_master extends Database
{
    public $id;
    public $date;
    public $customer_name;
    public $customer_contact;
    public $total_price;
    public $payment;
    public $due;
    public $new_total_payment;
    public $new_due;

    public function __construct(){

        parent:: __construct();
    }

    public function prepareData($data){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists("customer_name",$data)){
            $this->customer_name=$data['customer_name'];
        }
        if(array_key_exists("customer_contact",$data)){
            $this->customer_contact=$data['customer_contact'];
        }
        if(array_key_exists("payment",$data)){
            $this->payment=$data['payment'];
        }
        if(array_key_exists("total_price",$data)){
            $this->total_price=$data['total_price'];
        }
        if(array_key_exists("due",$data)){
            $this->due=$data['due'];
        }
        if(array_key_exists("new_total_payment",$data)){
            $this->new_total_payment=$data['new_total_payment'];
        }
        if(array_key_exists("new_due",$data)) {
            $this->new_due = $data['new_due'];
        }
        return $this;
    }
    public function insertData(){
        date_default_timezone_set('Asia/Dhaka');
        $date= date('Y-m-d H-i-s');
        $this->date=$date;
        $query= "INSERT INTO `bill_master`(`date`,`customer_name`, `customer_contact`, `total`, `payment`, `due` ) VALUES (?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->date);
        $STH->bindParam(2,$this->customer_name);
        $STH->bindParam(3,$this->customer_contact);
        $STH->bindParam(4,$this->total_price);
        $STH->bindParam(5,$this->payment);
        $STH->bindParam(6,$this->due);
        $STH->execute();
    }
    public function showPaid(){
        $sql = "SELECT * FROM bill_master WHERE due=0";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showUnPaid(){
        $sql = "SELECT * FROM bill_master WHERE due>0";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOneDetails(){
        $sql = "SELECT * FROM bill_master WHERE id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function updatePayment(){
        $query="UPDATE `bill_master` SET `payment`=?,`due`=? WHERE id=$this->id";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->new_total_payment);
        $STH->bindParam(2,$this->new_due);
        $STH->execute();
    }



}