<?php

include_once "Database.php";
class Login_info extends Database
{
    public $id;
    public $name;
    public $email;
    public $password;

    public function __construct(){

        parent:: __construct();
    }
    public function prepareData($data){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists("email",$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists("password",$data)){
            $this->password=md5($data['password']);
        }
        return $this;
    }

    public function login(){
        $query = "SELECT * FROM `login_info` WHERE `email`='$this->email' AND `password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();
        
        $count=$STH->rowCount();
        
        if($count>0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function user_info(){
        $query = "SELECT * FROM `login_info` WHERE `email`='$this->email' AND `password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function logged_in(){
        if ((array_key_exists('email', $_SESSION)) && (!empty($_SESSION['email']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function log_out(){
        $_SESSION['email']="";
        return TRUE;
    }
    
}