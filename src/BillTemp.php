<?php
include_once 'Database.php';
class BillTemp extends Database
{
    public $id;
    public $customer_name;
    public $customer_contact;
    public $product_id;
    public $quantity;
    public $price;
    public $total;

    public function __construct()
    {
        parent:: __construct();
    }

    public function prepareData($data){
        if(array_key_exists("customer_name",$data)){
            $this->customer_name=$data['customer_name'];
        }
        if(array_key_exists("customer_contact",$data)){
            $this->customer_contact=$data['customer_contact'];
        }
        if(array_key_exists("product_id",$data)){
            $this->product_id=$data['product_id'];
        }
        if(array_key_exists("quantity",$data)){
            $this->quantity=$data['quantity'];
        }
        if(array_key_exists("price",$data)){
            $this->price=$data['price'];
        }
        return $this;
    }
    public function insertData(){
        $this->total=$this->quantity*$this->price;
        $query= "INSERT INTO `billTemp`(`customer_name`, `customer_contact`, `product_id`, `quantity`, `price`, `total_price`) VALUES (?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->customer_name);
        $STH->bindParam(2,$this->customer_contact);
        $STH->bindParam(3,$this->product_id);
        $STH->bindParam(4,$this->quantity);
        $STH->bindParam(5,$this->price);
        $STH->bindParam(6,$this->total);
        $STH->execute();
    }
    public function if_exist(){
        $sql = "SELECT * FROM `billTemp`";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetch();
        $count=$STH->rowCount();

        if($count){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    public function showCustomer(){
        $sql = "SELECT customer_name,customer_contact FROM `billTemp` GROUP BY customer_name AND customer_contact";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showBillTemp(){
        $sql = "SELECT billTemp.*,product.product_name,unit_lookup.unit_name FROM `billTemp`,product,unit_lookup WHERE product.id=billTemp.product_id AND unit_lookup.id=product.unit_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function totalPrice(){
        $sql = "SELECT sum(total_price) as total_price FROM billTemp";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function delete(){
        $query="DElETE FROM billTemp";
        $this->DBH->exec($query);
    }


}
